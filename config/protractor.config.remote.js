const chai = require('chai');

module.exports = {
    allScriptsTimeout: 50000,
    seleniumAddress: 'http://hub.crossbrowsertesting.com:80/wd/hub',
    baseUrl: '[...]',
    // How long to wait for a page to load.
    getPageTimeout: 50000,
    maxSessions: 1,
    onPrepare: () => {
        expect = chai.expect;
        browser.ignoreSynchronization = true;
        browser.manage().timeouts().implicitlyWait(30000);
    },
    resultJsonOutputFile: 'report.json',
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),
    specs: [
        './src/features/*.feature',
    ],
    cucumberOpts: {
        require: './src/features/*/*.js',
        format: ['progress'],
        keepAlive: false,
    },

};
