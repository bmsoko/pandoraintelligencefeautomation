const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

module.exports = {
    allScriptsTimeout: 50000,
    seleniumAddress: 'http://localhost:4444/wd/hub',
    baseUrl: '[...]',
    // How long to wait for a page to load.
    getPageTimeout: 50000,
    maxSessions: 1,
    onPrepare: () => {
        chai.use(chaiAsPromised);
        // Disable Angular animations so e2e tests run more quickly
        browser.addMockModule('disableNgAnimate', () => {
            angular.module('disableNgAnimate', []).run($animate => {
                $animate.enabled(false);
            });
        });

        global.isAngularSite = flag => {
            browser.ignoreSynchronization = !flag;
        };
        browser.manage().timeouts().implicitlyWait(30000);
    },
    resultJsonOutputFile: 'report.json',
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),
    specs: [
        './src/features/*.feature',
    ],
    cucumberOpts: {
        strict: true,
        'no-colors': false,
        require: './src/features/*/*.js',
        format: ['progress','json:src/report/e2e-test-results.json'],
        keepAlive: false,
    },
    SELENIUM_PROMISE_MANAGER: false
};
