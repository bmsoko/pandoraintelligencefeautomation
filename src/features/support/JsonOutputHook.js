// /**
//  * This creates a JSON file that it's later going to be consumed by the gulp task test-e2e-report
//  */
// const Cucumber = require('cucumber');
// const fs = require('fs-extra');
// const path = require('path');

// const JsonFormatter = Cucumber.Listener.JsonFormatter();
// const reportPath = './src/report/src/';
// const reportFile = 'e2e-test-results.json';

// module.exports = function JsonOutputHook() {
//     JsonFormatter.log = (json) => {
//         if (!fs.existsSync(reportPath)) {
//             fs.mkdirSync(reportPath);
//         }

//         const destination = path.join(reportPath, reportFile);
//         fs.open(destination, 'w+', (err, fd) => {
//             if (err) {
//                 throw err;
//             } else {
//                 fs.writeSync(fd, json);
//             }
//         });
//     };

//     this.registerListener(JsonFormatter);
// };
