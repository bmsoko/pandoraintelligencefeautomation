'use strict';

const BasePage = require('../../pages/BasePage.js');
const chai = require('chai');
const EC = protractor.ExpectedConditions;
chai.config.truncateThreshold = 0; // disable truncating
const expect = chai.expect;


const {Before, BeforeAll, AfterAll, Given, Then, When} = require('cucumber');
    let basePage;

    Before(function () {
        basePage = new BasePage();
        
    });

    /*Given*/
    Given (/^user is on the login page$/, async () => {
        await basePage.go();
    });

    /*When*/

    /*Then*/