'use strict';

const BasePage = require('../../pages/BasePage.js');
const LoginPage = require('../../pages/LoginPage.js');
const chai = require('chai');
const EC = protractor.ExpectedConditions;
chai.config.truncateThreshold = 0; // disable truncating
var {setDefaultTimeout} = require('cucumber');


const {Before, BeforeAll, Given, Then, When} = require('cucumber');

const expect = chai.expect;
    let basePage;
    let loginPage;

    Before(() => {
        basePage = new BasePage();
        loginPage = new LoginPage();
        isAngularSite(false);
        setDefaultTimeout(60 * 1000);
    });

    /*Given*/

    /*When*/
    When(/^user logs in with (.*) username and (.*) password$/, async (usr, pwd) => {
        await loginPage.setCredentials(usr,pwd);
        await loginPage.submitLogin();
    });

    /*Then*/
   
