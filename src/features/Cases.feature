@viewer
Feature: Login
    As a user of the web page Pandora Intelligence
    I want to be able to login to the app
    So that I can create cases and use the application

    @smoke
    # @login
    Scenario: Verify that user can succesfully login
        Given user navigates to login page
        When user logs in with "tester" username and "test_1234" password
        Then user sees the cases page
