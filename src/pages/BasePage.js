'use strict';
this.shoadowDom = element(By.css('main-content'));

const BasePage = function BasePage() {
    /**
     * Function to navigate to the page set up in the baseUrl on the config file
     */
    this.go = async() => {
        await browser.get('');
        console.log('ingrese al go');
    };

    // Attempt to make shawdo DOM work with selenium.
    this.getExtShadowRoot = async () => {
        let shadowHost;
        await (shadowHost = browser.findElement(this.shoadowDom));
        return browser.executeScript("return arguments[0].shadowRoot",                                                                 shadowHost);
      };

       this.findShadowDomElement = async (shadowDomElement) => {
        let shadowRoot;
        let element;
        await (shadowRoot = this.getExtShadowRoot());
        await shadowRoot.then(async (result) => {
          await (element = result.findElement(By.css(shadowDomElement)));
        });
        
        return element;
      }



};
module.exports = BasePage;
