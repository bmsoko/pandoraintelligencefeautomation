'use strict';
const BasePage = require('../pages/BasePage.js');

const EC = protractor.ExpectedConditions;

const LoginPage = function LoginPage() {
    let basePage = new BasePage();

    this.usernameInput = element(By.name('username'));
    this.nextLoginBtn = element(By.buttonText('NEXT'));
    this.pwdInput = element(By.name('password'));
    this.signInBtn = element(By.buttonText('SIGN IN'));
    
    /* Methods */
     this.setCredentials = async function(username, password) {
         let usern;
        console.log('dentro y antes del setcredentials')
        //need to make shadowDOM to work with selenium
         await (usern = basePage.findShadowDomElement(this.usernameInput));
        
        console.log('Setting credentials, I was supposed to interact the usernname')
        await this.usernameInput.sendKeys(usern);
        console.log('dentro y despues del setcredentials')
        await browser.wait(EC.elementToBeClickable(this.nextLoginBtn));
        await this.nextLoginBtn.click();
        await browser.wait(EC.presenceOf(this.pwdInput));
        await this.pwdInput.sendKeys(password);
    };

    this.submitLogin =  async function() {
        await this.signInBtn.click();
    };
 
    };

 
module.exports = LoginPage;
