## Project Structure
Structure:

```
.../scripts/
    start-selenium.sh                       #Starts the selenium server
    test-e2e.sh                             #Calls the grunt task
.../src/
    e2e
        /pages                              # Pages Objects
            <PageName>.js
        /features                           # Cucumber histories
            <FatureToTest>.feature
            /Steps_Definitions              # Cucumber steps
                <FeatureToTest>_steps.js
package.json                                # Dependencies and scripts calls

```

##Framework Architecture

### Webdriver manager
Our framework uses Selenium Web Driver to interact with the application's components; it was developed to better support dynamic web pages where elements of a page may change without the page itself being reloaded.
Selenium-WebDriver makes direct calls to the browser using each browser’s native support for automation. How these calls are made, and the features they support depends on the browser you are using.

### Protractor
Protractor is an end-to-end testing framework for AngularJS applications and works as a solution integrator - combining powerful tools and technologies such as NodeJS, Selenium, webDriver, Jasmine, Cucumber and Mocha.
It has a bunch of customizations from Selenium to easily create tests for AngularJS applications.
Protractor also speeds up your testing as it avoids the need for a lot of “sleeps” and “waits” in your tests, as it optimizes sleep and wait times.

It runs on top of the Selenium, and this provides all the benefits and advantages from Selenium. In addition, it provides customizable features to test AngularJS applications. It is also possible to use some drivers which implement WebDriver's wire protocol like ChromeDriver and GhostDriver, as Protractor runs on top of the Selenium.

The file that tells protractor how to run is the `/test/protractor-<env>-conf.js`. We say `<env>` because we currently have two `.conf` files, one for running the tests on a local environment and another for running them on live environment. We are going to explain some sections that tell protractor where to look for the `.features` files (the ones that describe the scenarios to be tested) and tell which framework to use (here's where we see the link between Protractor and CucumberJS)

```
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),
    specs: [
        #Here is where we indicate the path to the .feature files
        'e2e/features/*.feature'        
    ],
    cucumberOpts: {
        #Here is where we indicate the path to the .js files holding the page object patterns and the coded steps
        require: './e2e/features/*/*.js',
        format: 'pretty',
        keepAlive: false //should stop test execution when test fails
    }
```    
In the next section you can see that there are several libraries being initialized, the most relevant, are the ones in charge of assertions (chai, chaiAsPromised, expect), they are being initialize here so they can be globally accessed from all the steps files (i.e: `library_steps.js`) allowing us to verify expected behavior; You may also notice, that there are some selenium commands to increase browser's timeout and to also maximize browser window during test execution.
Basically, you can think as this section as a environment preparation for the tests, please check [Reference Config](https://github.com/angular/protractor/blob/master/docs/referenceConf.js) for further explanation.

```
    onPrepare: function () {
        // All imports
        fs = require('fs');
        path = require('path');
        util = require('util');
        chai = require('chai');
        chaiAsPromised = require('chai-as-promised');
        chai.use(chaiAsPromised);
        expect = chai.expect;
        // Disable animations so e2e tests run more quickly
        browser.addMockModule('disableNgAnimate', function() {
            angular.module('disableNgAnimate', []).run(function($animate) {
                $animate.enabled(false);
            });
        });
        browser.ignoreSynchronization = true; //This is set for non-Angular apps
        browser.manage().timeouts().implicitlyWait(30000);
        browser.driver.manage().window().maximize();

    }
```
Pay attention to the line `browser.ignoreSynchronization = true;` this line allow us to work with any kind of application non just Angular, which is the case for our widget implementatinons!

### Config folder
The folder contains the configurations for the project.
All the configuration files are on this folder and you'll be able to differentiate the types by their names: `apps....config.js` are the ones with the URLs and `protractor....config.js` are the ones for Protractor.

Then, on the root directory, we have `protractor.config.js`, which is the only file Protractor is aware of. When the file is executed/read, it will check the environment variables and it will get and merge the files on the config folder depending on their values. The file checks for these variables:

- `NODE_ENV`: This is the standard variable all Node projects use to detect in which file is the project running. This will tell the script to require `protractor.base.config.js`, and based on the environment (local or browserstack), require the necessary file and merge it with the base template.
- `CONFIG_ENV`: This is the one we use for the data configuration. Based on its value (empty, staging or local), it will require the necessary `app....config.js`.

Task| Description
------------ | -------------
npm run e2e | protractor.base.config + protractor.local.config + apps.config
npm run e2e:browserstack | protractor.base.config + protractor.local.config + apps.config + apps.staging.config
npm run e2e:local | protractor.base.config + protractor.local.config + apps.config + apps.local.config



### Cucumber JS
It is a framework used for BDD, based on Gherkin. It allows you to write the tests as .feature files using the "given-when-then" syntax and match each step with the code that is going to be executed by protractor.

#### .feature sample test:

```
@smoke
Scenario: Authenticated user select an account
        Given user is on the login page
        When user sends correct access credentials
        And the user selects "modsquadqa" account on the list
        Then user sees that library is displayed
```
Each of these steps has to be coded so that Protractor can execute it via the web driver, so here is an example:

The @smoke is a tag, we have tags defined per feature and for test type (smoke or regression). While coding the tests you can add the tag that you want: @current, @wip, @blabla in order to only execute the tests you are currently working on.

#### sample step coded:
```
Then(/^user sees that library is displayed$/, async (done) => {
await expect(libraryPage.libraryBoxContainer.get(0).isDisplayed()).to.eventually.be.true.and.notify(done);
});
```
The **"expect"** is where you can see Protractor and Chai in action: this method basically resolves a promise that evetually the Media Library with the grid of photos is going to be visible.

### Page Objects Pattern
The functional tests follows the **Page-Object pattern**. In simple words:
The Page Object pattern represents the screens of your web app as a series of objects
and encapsulates the features represented by a page. It allows us to model the UI in our tests.

It Reduces the duplication of code,  makes tests more readable and robust and improves the maintainability of tests.

### Naming convention

#### Page Object:

```
javascript
'use strict';

const XXXXPage  = function XXXXPage() {
    // Mapping elements
    this.<PAGE_ELEMENT> = element.<SELECTOR>;

    // Methods & Documentation
    /**
     * DESCRIPTION OF WHAT THE METHOD DOES
     * @param element
     * @returns {*}
     */

     this.<METHOD_NAME_BY_ACTION> = (var1, var2, var3) => {
       .
       .
       .
       .
     };


    };

};
module.exports = XXXXPage;
```

#### Steps:
File name `XXXX_steps.js`

```
javascript
'use strict';

const chai = require('chai');
const XXXXPage = require('../../pages/XXXXXPage.js');
chai.config.truncateThreshold = 0; // disable truncating
const expect = chai.expect;


module.exports = function() {
    var xxxxPage;

    this.Before(function() {
        xxxxPage = new xxxxPage();
    });

    /*Given*/
    this.Given(/^<DESCRIPTION_OF_GIVEN_STEP>$/, (done) => {
        xxxxPage.go();
        browser.driver.wait(EC.visibilityOf(loginPage.emailInput));
        done();
    });

    /*When*/
    this.When(/^<DESCRIPTION_OF_WHEN_STEP>$/, (done) => {
        <METHODS_HERE>
        done();
    });

    /*Then*/
    this.Then(/^<DESCRIPTION_OF_THEN_STEP>$/, (done) => {
        expect(accountsPage.accountsList.get(0).isDisplayed()).
            to.eventually.be.equal(true, 'The account screen is not present').and.notify(done);
    });
};

```

### Reporting
TBC

## HowTo Execute tests

### Local execution

First of all the selenium server needs to be up and running, so:

`npm run selenium`

To run the tests:
`npm run e2e:local @<name_of_the_tag> --baseUrl <url_to_test>`


## Project Fundamentals
Usefull links for more information:
- [Protractor](http://angular.github.io/protractor/) *Test Platform*
- [Chai](http://http://chaijs.com/) *Assertion and language*
- [Cucumber](https://github.com/cucumber/cucumber-js)
- [Web Driver](http://www.seleniumhq.org/projects/webdriver/)
